import {Component, EventEmitter, Input, Output} from '@angular/core';

@Component({
  selector: 'app-catalog',
  templateUrl: './catalog.component.html',
  styleUrls: ['./catalog.component.sass']
})
export class CatalogComponent {
  @Input() products;
  @Output() toCart: EventEmitter<any> = new EventEmitter();

  addToCart(item) {
    this.toCart.emit(item);
  }

}
