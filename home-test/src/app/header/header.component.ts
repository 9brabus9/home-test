import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.sass']
})
export class HeaderComponent implements OnInit {
  @Input()
  cartProducts;
  @Input()
  products;
  public open = false;
  public a: string;
  public category: string;
  public openCategory = false;
  public filteredCategory;

  public categorySearch = [
    {name: 'По умолчанию',product: 'all'},
    {name: 'Названию',product: 'product_name'},
    {name: 'Артикулу',product: 'code'},
    {name: 'Бренду', product: 'brand'},
  ];

  ngOnInit() {

  }

  openCart() {
      this.open = !this.open;
  }

  deleteFromCart(event: any) {
    this.cartProducts.splice(event, 1);
  }
  categorySelect(target){
    this.category = target.innerHTML;
    this.filteredCategory = this.categorySearch.filter(product => product.name === this.category );
    this.filteredCategory = this.filteredCategory[0].product;
  }
  openCategorySearch() {
    this.openCategory = !this.openCategory;
  }

}
