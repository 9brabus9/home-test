import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
type Product = {
  product_name: string;
  code: string,
  brand: string,
  price: number
}

@Injectable()

export class ProductService {

  constructor (private http: HttpClient) {}

  fetchProducts (): Observable<Product[]> {
    return this.http.get<Product[]>('assets/data/product?code="1045"');

  }
}



