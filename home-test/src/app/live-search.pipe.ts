import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'liveSearch'
})
export class LiveSearchPipe implements PipeTransform {

  transform(value: any, a?: any): any {
    a[0] = a[0].replace(/^\s*/,'');
    a[0] = a[0].toLowerCase();
    if(a[1]==='all') {
      return value.filter(item => (item.product_name.toLowerCase().indexOf(String(a[0])) !== -1 || item.code.toLowerCase().indexOf(String(a[0])) !== -1 || item.brand.toLowerCase().indexOf(String(a[0])) !== -1));
    }
    return value.filter(item => item[a[1]].toLowerCase().indexOf(String(a[0])) !== -1);
  }

}
