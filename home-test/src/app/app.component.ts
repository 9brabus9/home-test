import { Component } from '@angular/core';
import { ProductService } from './product.service';
import { Observable } from 'rxjs/Observable';
import {OnInit} from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.sass']
})
export class AppComponent implements OnInit {
  constructor(private products: ProductService) {

  }
  ngOnInit() {
    this.products$ = this.products.fetchProducts();
  }
  public products$: Observable<Product[]>;
  public cartProducts = [];

  addToCart(event: any) {
    this.cartProducts.push(event);
  }
}
type Product = {
  product_name: string;
  code: string,
  brand: string,
  price: number
}
